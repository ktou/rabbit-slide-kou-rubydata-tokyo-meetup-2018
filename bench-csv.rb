#!/usr/bin/env ruby

require "benchmark"
require "arrow"
require "ccsv"
require "open-uri"

path = "geoip.csv"
unless File.exist?(path)
  open("https://github.com/vonconrad/csv-benchmark/raw/master/csv/geoip.csv") do |input|
    open(path, "w") do |output|
      IO.copy_stream(input, output)
    end
  end
end

Benchmark.bmbm do |x|
  x.report("csv") do
    CSV.foreach(path) {|row| row}
  end

  x.report('ccsv') do
    Ccsv.foreach(path) {|row| row}
  end

  x.report("arrow") do
    Arrow::Table.load(path, use_threads: true)
  end
end
