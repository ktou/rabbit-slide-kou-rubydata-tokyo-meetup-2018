= Apache Arrow

Apache Arrowの2018年11月現在の最新情報を紹介します。特に、Ruby関連の部分を紹介します。

== ライセンス

=== スライド

CC BY-SA 4.0

原著作者名は以下の通りです。

  * 須藤功平（またはKouhei Sutou）

=== 画像

==== クリアコードのロゴ

CC BY-SA 4.0

原著作者：株式会社クリアコード

ページヘッダーで使っています。

== 作者向け

=== 表示

  rake

=== 公開

  rake publish

== 閲覧者向け

=== インストール

  gem install rabbit-slide-kou-rubydata-tokyo-meetup-2018

=== 表示

  rabbit rabbit-slide-kou-rubydata-tokyo-meetup-2018.gem

