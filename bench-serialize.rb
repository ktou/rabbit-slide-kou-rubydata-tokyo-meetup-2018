#!/usr/bin/env ruby

require "benchmark"
require "tempfile"
require "arrow"
require "json"
require "csv"

n = 1000000
numbers = n.times.to_a
arrow_table = Arrow::Table.new("number" => Arrow::Int32Array.new(numbers))

tmp_dir = "/dev/shm"
unless File.directory?(tmp_dir)
  tmp_dir = nil
end

Benchmark.bmbm do |benchmark|
  benchmark.report("JSON") do
    json_file = Tempfile.new(["bench-serialize", ".json"], tmp_dir)
    json_file.open
    JSON.dump(numbers, json_file)
    json_file.close
    json_file.open
    JSON.load(json_file)
    json_file.close
  end

  benchmark.report("CSV") do
    csv_file = Tempfile.new(["bench-serialize", ".csv"], tmp_dir)
    CSV.open(csv_file.path, "w") do |csv|
      numbers.each do |number|
        csv << [number]
      end
    end
    CSV.read(csv_file.path)
  end

  benchmark.report("Arrow") do
    arrow_file = Tempfile.new(["bench-serialize", ".arrow"], tmp_dir)
    arrow_table.save(arrow_file.path)
    Arrow::Table.load(arrow_file.path)
  end
end
