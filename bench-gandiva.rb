#!/usr/bin/env ruby

require "benchmark"
require "numo/narray"
require "daru"
require "gandiva"

n = 100000
ruby_table = n.times.collect do
  {
    "number1" => rand,
    "number2" => rand,
  }
end

number1 = []
number2 = []
ruby_table.each do |record|
  number1 << record["number1"]
  number2 << record["number2"]
end

numo_number1 = Numo::DFloat[number1]
numo_number1.reshape(numo_number1.shape[-1])
numo_number2 = Numo::DFloat[number2]
numo_number2.reshape(numo_number2.shape[-1])

daru_data_frame = Daru::DataFrame.new("number1" => number1,
                                      "number2" => number2)

arrow_table = Arrow::Table.new("number1" => Arrow::DoubleArray.new(number1),
                               "number2" => Arrow::DoubleArray.new(number2))

Benchmark.bmbm do |benchmark|
  benchmark.report("Array") do
    ruby_table.collect do |record|
      record["number1"] + record["number2"]
    end
  end

  benchmark.report("Numo::NArray") do
    numo_number1 + numo_number2
  end

  benchmark.report("Daru") do
    daru_data_frame["number1"] + daru_data_frame["number2"]
  end

  benchmark.report("Arrow") do
    schema = arrow_table.schema
    expression =
      Gandiva::Expression.new("add",
                              [schema[:number1], schema[:number2]],
                              Arrow::Field.new("sum", :double))
    projector = Gandiva::Projector.new(schema, [expression])
    arrow_table.each_record_batch do |record_batch|
      projector.evaluate(record_batch)
    end
  end
end
